﻿=== chapter1 ===
= start
-> puppyChoices

= puppyChoices

I found a puppy in this basement
* [Love puppies] 
    I love puppies!
* [Kick puppies]
    I kick puppies. Every day.
* [I'm busy]
    Surely there's more important stuff going on?
* [What puppies?]
    There aren't any puppies any more!
