﻿VAR money = 0

You'd love to go in that tent, but you're {describeWealth(money)}

=== oldMan ===
= interact
	{ knight.hit:
		{ rewardForHittingKnight:
			Thanks, kid!
	    	-else:
	    	-> rewardForHittingKnight
		}
	    -else: 
		-> tellAboutKnight
	}
	->END

= tellAboutKnight
	I can't stand that knight! Acting like he's the boss of everyone.
	I wish someone would teach him a lesson. Hey, kid, you look like you could handle it!
	-> END

= rewardForHittingKnight
	Haha! Oh man, the look on his face! Here's a reward for you.
	{addMoney(1)}
	You're {describeWealth(money)}
	->END



=== knight ===
= interact
	{ not hit:
    	-> hit
    	-else:
    	-> talkAfterHitting
	}

= hit
	Scram, punk!
	[You hit the knight. The old man looks impressed.]
	->END

= talkAfterHitting
	You again! Don't hurt me!
	->END



=== carny ===
= interact
	{ money == 0:
		->tellPrice
		-else:
		Thanks kid! Come on in!
	}
	->END

= tellPrice
	{ tellPrice == 1:
		You want in? One gold coin.
		-else:
		I already told you kid! One gold coin!
	}
	[You check your pockets, you're {tellPrice > 1: still }{describeWealth(money)}]
	Come back when you've got some money, kid.
	->END





=== function addMoney(x) ===
~ money = money + x
~ return "[You've got "+money+" coins]"

=== function describeWealth(x) ===
{
- x == 0:
    ~ return "penniless!"
- else:
    ~ return "rich enough to enter the tent!"
}