﻿//Scales the orthographic size by width or height to fit orthographic size and a set screen ratio

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class AspectRatioCamera : MonoBehaviour {
	public new Camera camera {
		get {
			Camera _camera = gameObject.GetComponent<Camera>();
			if(_camera == null) Debug.Log("Camera not found");
			return _camera;
		}
	}

	public Vector2 aspectRatio = new Vector2(4,3);
	public float size = 1;

	private void Update () {
	    float targetAspect = aspectRatio.x/aspectRatio.y;
	    // determine the game window's current aspect ratio
	    float aspect = (float)Screen.width / (float)Screen.height;
	    // current viewport height should be scaled by this amount
	    float scaleHeight = aspect / targetAspect;
	    // if scaled height is less than current height, letterbox screen size
	    
	    if (scaleHeight < 1.0f) {
			camera.orthographicSize = size / scaleHeight;
	    } else {
			camera.orthographicSize = size;
	    }
	}
}
