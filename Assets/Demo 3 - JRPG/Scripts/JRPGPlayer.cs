﻿using UnityEngine;
using System.Collections;

public class JRPGPlayer : MonoBehaviour {
	public JRPGStory story;
	public Rigidbody2D rigidbody {
		get {
			return GetComponent<Rigidbody2D>();
		}
	}
	public float walkSpeed = 1;
	public JRPGNPC targetNPC;

	void Update () {
		story.displayTop = Camera.main.WorldToViewportPoint(transform.position).y < 0.5f;
		if(!story.reading && Input.GetKeyDown(KeyCode.Space)) {
			if(targetNPC != null)
				targetNPC.Interact();
		}
	}

	void FixedUpdate () {
		if(story.reading) 
			return;
		Vector2 walkVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized * walkSpeed;
		rigidbody.MovePosition(rigidbody.position + walkVector * Time.fixedDeltaTime);
	}

	void OnTriggerEnter2D (Collider2D collider) {
		JRPGNPC currentTargetNPC = collider.transform.parent.GetComponent<JRPGNPC>();
		if(currentTargetNPC != null)
			targetNPC = currentTargetNPC;
    }

	void OnTriggerExit2D (Collider2D collider) {
		if(targetNPC == null) 
			return;
		JRPGNPC currentTargetNPC = collider.transform.parent.GetComponent<JRPGNPC>();
		if(currentTargetNPC == targetNPC)
			targetNPC = null;
	}
}