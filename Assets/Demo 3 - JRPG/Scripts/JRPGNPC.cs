using UnityEngine;
using System.Collections;

public class JRPGNPC : MonoBehaviour {
	public JRPGStory story;
	public string inkPath = "";

	public void Interact () {
		story.story.ChoosePathString(inkPath);
		story.RefreshView();
	}
}