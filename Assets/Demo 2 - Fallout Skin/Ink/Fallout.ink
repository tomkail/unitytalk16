﻿Nick: I found a dog outside.
Nick: It looks pretty hungry
* [Let's see!] I <i>love</i> dogs! Let's go see.
	Nick: I knew you'd say that!
* [Dead already] Then it's already dead. I've got nothing to spare.
	Nick: That's heartless. What's <i>wrong</i> with you? Look, I'm going to give it some food.
	Player: Fine. 
* [I'm busy] Listen, Nick; we've got way more important stuff to do.
	Nick: <i>Nobody</i> is too busy for dogs. We're going.
* [A dog?] You found a dog <i>here</i>? This place is a warzone!
	Nick: I sure did! Let's go see it!

- Dog: <i>*Woof*</i>
Nick: Wow, it sure is happy to see you!