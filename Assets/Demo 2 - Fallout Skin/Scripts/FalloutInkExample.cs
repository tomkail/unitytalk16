﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ink.Runtime;

public class FalloutInkExample : MonoBehaviour {
	[SerializeField]
	private TextAsset inkJSONAsset;
	private Story story;

	[SerializeField]
	private Canvas canvas;
	[SerializeField]
	private Image player;
	[SerializeField]
	private Image nick;
	[SerializeField]
	private Image dog;
	private Image currentSpeaker;

	[SerializeField]
	private Text text;
	[SerializeField]
	private FalloutChoiceButton[] buttons;

	private bool waitingOnInput;

	void Awake () {
		StartStory();
	}

	void StartStory () {
		InitSpeakers();
		story = new Story (inkJSONAsset.text);
		RefreshView();
	}

	void Update () {
		if(!waitingOnInput && Input.GetMouseButtonDown(0)) {
			if(story.canContinue) {
				RefreshView();
			} else if(story.currentChoices.Count > 0) {
				RefreshView();
			} else {
				Debug.Log("Reached end of story.");
//				StartStory();
			}
		}
	}
	void RefreshView () {
		if (story.canContinue) {
			HideChoices();
			text.gameObject.SetActive(true);
			string rawText = story.Continue ().Trim();
			text.text = ParseContent(rawText);
		} 
		else if(story.currentChoices.Count > 0) {
			ChangeSpeaker("Player");
			for (int i = 0; i < Mathf.Min(story.currentChoices.Count, buttons.Length); i++) {
				FalloutChoiceButton button = buttons[i];
				Choice choice = story.currentChoices[i];
				button.gameObject.SetActive(true);
				button.text.text = choice.text;
				button.button.onClick.AddListener(delegate{
					OnClickChoiceButton(choice);
				});
			}
			waitingOnInput = true;
		}
	}

	public string ParseContent (string rawContent) {
		string subjectID = "";
		string content = "";
		if(!TrySplitContentBySearchString(rawContent, ": ", ref subjectID, ref content)) return rawContent;
		ChangeSpeaker(subjectID);
		return content;
	}

	public bool TrySplitContentBySearchString (string rawContent, string searchString, ref string left, ref string right) {
		int firstSpecialCharacterIndex = rawContent.IndexOf(searchString);
		if(firstSpecialCharacterIndex == -1) return false;
		
		left = rawContent.Substring(0, firstSpecialCharacterIndex).Trim();
		right = rawContent.Substring(firstSpecialCharacterIndex+searchString.Length, rawContent.Length-firstSpecialCharacterIndex-searchString.Length).Trim();
		return true;
	}

	void ShowContentView (string text) {
		
	}

	void ShowChoiceView (string text) {
		
	}

	void HideChildren () {
		
	}

	void HideContent () {
		text.gameObject.SetActive(false);
	}

	void HideChoices () {
		foreach(var button in buttons) {
			button.gameObject.SetActive(false);
		}
	}

	public void OnClickChoiceButton (Choice choice) {
		for (int i = 0; i < Mathf.Min(story.currentChoices.Count, buttons.Length); i++) {
			FalloutChoiceButton button = buttons[i];
			button.button.onClick.RemoveAllListeners();
		}
		waitingOnInput = false;
		story.ChooseChoiceIndex (choice.index);
		RefreshView();
	}

	private void InitSpeakers () {
		player.enabled = false;
		nick.enabled = false;
		dog.enabled = false;
		ChangeSpeaker("Player");
	}

	private void ChangeSpeaker (string speaker) {
		if(currentSpeaker != null)
			currentSpeaker.enabled = false;
		if(speaker == "Nick") {
			currentSpeaker = nick;
		} else if(speaker == "Dog") {
			currentSpeaker = dog;
		} else {
			currentSpeaker = player;
		}
		currentSpeaker.enabled = true;
	}
}
