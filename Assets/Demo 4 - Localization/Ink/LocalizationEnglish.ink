﻿VAR stamina = 5

-> main

=== main ===

= start
	After inumerable traffic jams, delayed flights and angry bus drivers, you finally reach Unite!
	-> mainHall

= mainHall
	From the main hall, you relax with a {&fresh coffee|mint tea|glass of water} and watch the crowds.
	{stamina > 0:
		+ Talk to someone!
			-> talkToSomeone
		* Present your talk!
			-> presentTalk
		+ Have lunch!
			-> haveLunch
	}
	+ Go back to the hotel!
		-> hotel

= talkToSomeone
	Somebody you recognise from Twitter introduces themselves, and you have a conversation about...
	+	[Games]
		You both do increasingly awful impressions of the merchant in Resident Evil 4 before reminiscing on the good old days of gaming.
	+	[Sandwiches]
		You explain how the BLT represents the pinnacle of sandwich cuisine before realizing that it's perhaps more of a British thing.
	+	[19th century dueling]
		They're and expert on the subject, and you walk away with some new, albeit slightly useless, knowledge.
	- {changeStamina(-1)}
	-> mainHall

= presentTalk
	You sweat bullets for 5 minutes before flubbing your lines and falling off stage.
	At least it's over now.
	{changeStamina(-2)}
	-> mainHall

= haveLunch
	The queue is ridiculous, but the food is free and filling.
	{changeStamina(1)}
	-> mainHall

= hotel
	You stumble into your hotel room and fall asleep before your body has even hit the bed.
	~ stamina = 5
	You wake up, feeling refreshed.
	-> mainHall

=== function changeStamina(x) ===
~ stamina = stamina + x
~ return describeStamina()

=== function describeStamina() ===
{
	- stamina == 0:
    	~ return "You're absolutely exhasted."
	- stamina == 1:
    	~ return "You're very sleepy."
	- stamina == 2:
		~ return "You're starting to get tired."
	- stamina == 3:
		~ return "You're feeling fine."
	- else:
    	~ return "You're full of energy!"
}