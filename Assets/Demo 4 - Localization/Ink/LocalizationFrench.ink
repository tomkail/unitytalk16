﻿// Dear French speakers - Pardon pour le translation.
VAR stamina = 5

-> main

=== main ===

= start
	Après les embouteillages inumerable, vols retardés et les chauffeurs de bus en colère, vous atteignez enfin Unite!
	-> mainHall

= mainHall
	De la salle principale , vous détendre avec un {&frais café|thé à la menthe|verre d'eau} et regarder les foules.
	{stamina > 0:
		+ Parler à quelqu'un!
			-> talkToSomeone
		* Présentez votre discours!
			-> presentTalk
		+ Déjeuner!
			-> haveLunch
	}
	+ Retour à l'hôtel!
		-> hotel

= talkToSomeone
	Quelqu'un vous reconnaissez de Twitter s'introduit , et vous avez une conversation sur...
	+	[Des Jeux]
		Vous avez tous deux fait des impressions plus terribles du marchand dans Resident Evil 4 avant remémorant le bon vieux temps de jeu.
	+	[Des Sandwichs]
		Vous expliquez comment le BLT représente le summum de la cuisine sandwich avant de réaliser qu'il est peut-être plus d'une chose britannique.
	+	[19ème siècle dueling]
		Ils sont et expert sur le sujet, et vous repartez avec une nouvelle, quoique inutile, connaissance.
	- {changeStamina(-1)}
	-> mainHall

= presentTalk
	Vous transpirez des balles pendant 5 minutes avant flubbing vos lignes et de tomber hors de la scène.
	At least it's over now.
	{changeStamina(-2)}
	-> mainHall

= haveLunch
	La file d'attente est ridicule, mais la nourriture est libre et le remplissage.
	{changeStamina(1)}
	-> mainHall

= hotel
	Vous tombez dans votre chambre d'hôtel et endormez avant que votre corps a même frappé le lit.
	~ stamina = 5
	Vous vous réveillez , une sensation de fraîcheur.
	-> mainHall

=== function changeStamina(x) ===
~ stamina = stamina + x
~ return describeStamina()

=== function describeStamina() ===
{
	- stamina == 0:
    	~ return "Vous avez tout à fait exhasted."
	- stamina == 1:
    	~ return "Vous êtes très somnolent"
	- stamina == 2:
		~ return "Vous commencez à être fatigué."
	- stamina == 3:
		~ return "Vous vous sentez bien."
	- else:
    	~ return "Vous êtes plein d'énergie!"
}