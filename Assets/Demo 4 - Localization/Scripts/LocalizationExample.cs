﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ink.Runtime;

// Switching languages resets the story, and loads the story file associated with that language.
// The catch is that you can't save/load the state of the story as easily because each language file might have a slghtly different structure, variable names, etc.
// If you wanted to do this, you would either need to keep the same structure (all the same names for stitch, variable, etc), and then load the same state file.
public class LocalizationExample : MonoBehaviour {

	public enum Language {
		English,
		French
	}

	public Language language;

	[SerializeField]
	private TextAsset englishInkJSONAsset;
	[SerializeField]
	private TextAsset frenchInkJSONAsset;

	private Story story;

	[SerializeField]
	private Transform parent;

	// UI Prefabs
	[SerializeField]
	private Text textPrefab;
	[SerializeField]
	private Button buttonPrefab;

	void Awake () {
		StartStory(LanguageToStory(language));
	}

	public void OnClickEnglishLanguageButton () {
		ChangeLanguage(Language.English);
	}

	public void OnClickFrenchLanguageButton () {
		ChangeLanguage(Language.French);
	}

	public void ChangeLanguage (Language language) {
		string storyStateJSON = story.state.ToJson();
		this.language = language;
		StartStory(LanguageToStory(language), storyStateJSON);
	}

	private string LanguageToStory (Language language) {
		if(language == Language.English)
			return englishInkJSONAsset.text;
		else if(language == Language.French)
			return frenchInkJSONAsset.text;
		Debug.LogError("Language "+language+" not recognized.");
		return englishInkJSONAsset.text;
	}

	void StartStory (string storyJSON, string stateJSON = null) {
		story = new Story (storyJSON);
		if(stateJSON != null)
			story.state.LoadJson(stateJSON);
		RefreshView();
	}

	void RefreshView () {
		RemoveChildren ();

		while (story.canContinue) {
			string text = story.Continue ().Trim();
			CreateContentView(text);
		}
//		CreateContentView(story.text);
		story.ContinueMaximally();
		if(story.currentChoices.Count > 0) {
			for (int i = 0; i < story.currentChoices.Count; i++) {
				Choice choice = story.currentChoices [i];
				Button button = CreateChoiceView (choice.text.Trim ());
				button.onClick.AddListener (delegate {
					OnClickChoiceButton (choice);
				});
			}
		} else {
			Button choice = CreateChoiceView("End of story.\nRestart?");
			choice.onClick.AddListener(delegate{
				StartStory(LanguageToStory(language));
			});
		}
	}

	void OnClickChoiceButton (Choice choice) {
		story.ChooseChoiceIndex (choice.index);
		RefreshView();
	}

	void CreateContentView (string text) {
		Text storyText = Instantiate (textPrefab) as Text;
		storyText.text = text;
		storyText.transform.SetParent (parent, false);
	}

	Button CreateChoiceView (string text) {
		Button choice = Instantiate (buttonPrefab) as Button;
		choice.transform.SetParent (parent, false);

		Text choiceText = choice.GetComponentInChildren<Text> ();
		choiceText.text = text;

		HorizontalLayoutGroup layoutGroup = choice.GetComponent <HorizontalLayoutGroup> ();
		layoutGroup.childForceExpandHeight = false;

		return choice;
	}

	void RemoveChildren () {
		int childCount = parent.childCount;
		for (int i = childCount - 1; i >= 0; --i) {
			GameObject.Destroy (parent.GetChild (i).gameObject);
		}
	}
}
